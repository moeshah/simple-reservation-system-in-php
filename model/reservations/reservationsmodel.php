<?php

class reservationsmodel extends database {

    function __construct() {
        parent::__construct();
    }

    function getAllReservations() {
        $base = 'select * from reservations';
        return $this->get_data_set($base, NULL);
    }

    function getReservationByRef($ref) {
        $base = 'select *, 
                DATEDIFF(resdepdate,resarrdate) as days, 
                (DATEDIFF(resdepdate,resarrdate) * ramount) as TotalFee
                from reservations 
                join rooms on rid = resrid 
                join customer on rescsid = csid where resref = :ref;';
        $params = [':ref' => $ref];
        return $this->get_data_set($base, $params);
    }

    function updateReservationCommentsById($resid, $comments) {
        $base = 'update reservations set rescomments = :comments where resid = :resid limit 1';
        $params = [':resid' => $resid, ':comments' => $comments];
        return $this->update_db($base, $params);
    }

    function createReservation($roomType, $csid, $terms, $arrdate, $depDate, $deparrival, $pType, $ref) {
        $base = 'INSERT INTO reservations (resref, rescsid, resrid, resarrdate, resdepdate, resptype, resterms, resdeparrival, rescancelled, rescreateddate) VALUES (:ref, :csid, :roomType, :arrdate, :depdate, :pType, :terms, :deparrival, "0", NOW())';
        $params = [':ref' => $ref, ':csid' => $csid, ':roomType' => $roomType, ':arrdate' => $arrdate, ':depdate'=>$depDate, ':pType'=>$pType, ':terms'=>$terms, ':deparrival'=>$deparrival];
        
        return $this->update_db($base, $params);
    }

    function __destruct() {
        parent::__destruct();
    }

}
