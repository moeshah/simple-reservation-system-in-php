<?php

class customermodel extends database {

    function __construct() {
        parent::__construct();
    }

    function getAllCustomers() {
        $basecommand = 'select * from customer';
        return $this->get_data_set($basecommand, NULL);
    }
    
    function getCustomerbyEmail($email){
        $base = 'select * from customer where csemail = :email;';
        $params = [':email' => $email];
        return $this->get_single_row($base, $params);
    }
    
    function createCustomer($fname, $lname, $email, $contact){
        $basecommand = 'INSERT INTO customer (csfirstname, cslastname, csemail, csmobile) VALUES (:fname, :lname, :email, :contact);';
        $params = [':fname' => $fname, ':lname' => $lname, ':email' => $email, ':contact' => $contact];
        return $this->update_db($basecommand, $params);
    }

    function __destruct() {
        parent::__destruct();
    }

}