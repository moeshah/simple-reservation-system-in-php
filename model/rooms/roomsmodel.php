<?php

class roomsmodel extends database {

    function __construct() {
        parent::__construct();
    }

    function getAllRooms() {
        $base = 'select * from rooms';
        return $this->get_data_set($base, NULL);
    }

    function getAvailableRooms($from, $to) {
        $base = 'SELECT r.rid,r.rname,r.ramount,r.rcapacity
    FROM
    rooms r
    LEFT JOIN reservations b
    ON b.resrid = r.rid
        AND
        (:fromdate BETWEEN b.resarrdate AND b.resdepdate
         OR           
         :todate BETWEEN b.resarrdate AND b.resdepdate
         OR  
         b.resarrdate BETWEEN :fromdate AND :todate)
    WHERE b.resrid IS NULL';
        $params = array(':fromdate' => $from, ':todate' => $to);
        return $this->get_data_set($base, $params);
    }

    function __destruct() {
        parent::__destruct();
    }

}
