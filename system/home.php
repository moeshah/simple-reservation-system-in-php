<?php

require_once '../core/core.php';

$CSSFile = '<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">';
$CSSFile .= '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">';
$JSFiles = '<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>';
$JSFiles .= '<script src="../assets/js/home.js?ver=' . time() . '" type="text/javascript"></script>';
require_once 'common/header.php';
?>
<style>
    .table td, th {
        text-align: center;   
    }

    .dataTables_filter {
        float: right;
    }

    .card{
        background-color: transparent;
        border: none;
    }

    @media screen and (max-width: 767px) {
        .bottom-space {
            padding-bottom: 50px;
        }
    }
</style>
<section id="home">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Specific Design for ResRequest Project</h2>
                <button type="button" class="btn btn-info" id="btnshowDbtbls">View All DB Tables</button>
                <div id="tbls" style="display:none">
                    <div class="col-md-12">
                        <h4 class="text-center">Rooms Table</h4>
                        <div class="table-responsive"> 
                            <table id="tblrooms" class="table display row-border cell-border table-bordered dt-responsive" style="width:100%;">
                                <thead>
                                    <tr>
                                        <td>Room ID</td>
                                        <td>Room Name</td>
                                        <td>Room Capacity</td>
                                        <td>Room Amount</td>
                                    </tr>    
                                </thead>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-12">
                        <div class="table-responsive"> 
                            <h4 class="text-center">Reservations Table</h4>
                            <table id="tblreservations" class="table display row-border cell-border table-bordered dt-responsive" style="width:100%;">
                                <thead>
                                    <tr>
                                        <td>Reservation ID</td>
                                        <td>Reservation Reference</td>
                                        <td>Reservation Customer ID</td>
                                        <td>Reservation Room ID</td>
                                        <td>Reservation Arrival Date</td>
                                        <td>Arrival and Departure T&c's</td>
                                        <td>Reservation Departure Date</td>
                                        <td>Accepted Terms</td>
                                        <td>Reservation Comments</td>
                                        <td>Reservation Cancelled</td>
                                        <td>Reservation Created Date</td>
                                    </tr>    
                                </thead>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-12">
                        <div class="table-responsive"> 
                            <h4 class="text-center">Customers Table</h4>
                            <table id="tblcustomer" class="table display row-border cell-border table-bordered dt-responsive" style="width:100%;">
                                <thead>
                                    <tr>
                                        <td>Customer ID</td>
                                        <td>Customer First Name</td>
                                        <td>Customer Last Name</td>
                                        <td>Customer Email</td>
                                        <td>Customer Mobile</td>
                                    </tr>    
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="reservations" class="bg-light">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fab fa-searchengin"></i></span>
                        </div>
                        <input class="form-control" type="text" placeholder="Search my reservation" aria-describedby="srchHelp" id="srchphrase">

                    </div>
                    <small id="srchHelp" class="form-text text-muted">Press Enter to search for your Booking</small>
                    <div class="errHolder"></div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <div id="srchres" style="display:none;">
                        <h4 class="text-center">Your Booking Details</h4>
                        <div class="table-responsive"> 
                            <table id="tblref" class="table display row-border cell-border table-bordered dt-responsive" style="width:100%;">
                                <thead>
                                    <tr>
                                        <td>Name</td>
                                        <td>Room Booked</td>
                                        <td>Arrival Date</td>
                                        <td>Departure Date</td>
                                        <td style="width:5%">Payment Type</td>
                                        <td>Comments</td>
                                        <td>Total Amount</td>
                                        <td style="width:5%">Action</td>
                                    </tr>    
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 ">
                <h4 class="text-center"><u>Book A Bed</u></h4>
                <form id="frmbookabed" method="post" autocomplete="off" >
                    <input type="hidden" id="txtfrom" name="txtfrom" value>
                    <input type="hidden" id="txtto" name="txtto" value>
                    <div class="col-md-12 mx-auto card">
                        <div class="form-group text-center">
                            <label class="text-center">Please select an Arrival and Departure date</label>
                            <div class="form-row form-group">
                                <div class="col">
                                    <label for="arrdate">Arrival Date:</label>
                                    <input type="text" class="form-control datepicker" placeholder="YYYY-MM-DD" id="txtarrdate">
                                </div>
                                <div class="col">
                                    <label for="depdate">Departure Date:</label>
                                    <input type="text" class="form-control datepicker" placeholder="YYYY-MM-DD" id="txtdepdate">
                                </div>
                            </div>
                            <div class="errorDates"></div> 
                            <input type="button" class="btn btn-info" id="btnRoomSearch" value="Search for Rooms"/>
                        </div>
                        <div class="crddetails" style="display:none;">

                            <div class="form-group text-center">
                                <label class="text-center" aria-describedby="roomtype" id="lblroomtype"></label>
                                <select class="custom-select my-1 mr-sm-2" id="sltroomtype" name="sltroomtype" data_val_type="select">
                                    
                                </select>
                                <small id="roomtype" class="form-text text-muted">*Available Rooms are shown for your selected dates</small>
                            </div>

                            <br>

                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <label for="firstname">First Name:</label>
                                        <input type="text" name="fname" class="form-control" id="txtfirstname" placeholder="Enter your First Name" data_val_type="name" data_val_minlength="2">
                                    </div>
                                    <div class="col">
                                        <label for="lastname">Last Name:</label>
                                        <input type="text" name="lname" class="form-control" id="txtlastname" placeholder="Enter your Last Name" data_val_type="surname" data_val_minlength="2">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <label for="email">Email address:</label>
                                        <input type="email" name="email" class="form-control" id="txtemail" aria-describedby="emailHelp" placeholder="Enter your email address" data_val_type="email">
                                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                    </div>
                                    <div class="col">
                                        <label for="txtcontact">Contact Number:</label>
                                        <input type="text" class="form-control" name="contactno" id="txtcontact" aria-describedby="contactHelp" placeholder="Enter your Contact Number" data_val_type="cellno">
                                        <small id="contactHelp" class="form-text text-muted">*Try +27815014527, More than 10 digits, less than 10 digits</small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-rdr">
                                <label for="rdr">Payment Method: </label>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline1" name="rdrInline" value="Credit Card" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadioInline1">Credit Card</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline2" name="rdrInline" value="Debit Card" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadioInline2">Debit Card</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline3" name="rdrInline" value="EFT" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadioInline3">EFT</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline4" name="rdrInline" value="Cash" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadioInline4">Cash on Arrival (We cannot Guarantee your room)</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <small>Recaptcha Goes here, However v3 is an invisible recaptcha so it will not take up any space.</small>
                            </div>

                            <!--                    Commented out due to another validation style being used, the data_val_type needs to be removed to not initialise the validation.
                                                    <div class="form-group">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="chkdeparrival" data_val_type="tcs">
                                                            <label class="custom-control-label" for="chkdeparrival">Check In Time: 1PM, Departure Time: 11PM (Late checkout on request)</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="chkTerms" data_val_type="tcs">
                                                            <label class="custom-control-label" for="chkTerms">I accept/read all terms and conditions stipulated by this company name</label>
                                                        </div>
                                                    </div>-->
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="chkdeparrival" name="chkdeparrival">
                                    <label class="custom-control-label" for="chkdeparrival">Check In Time: 1PM, Departure Time: 11PM (Late checkout on request)</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="chkTerms" name="chkTerms">
                                    <label class="custom-control-label" for="chkTerms">I accept/read all terms and conditions stipulated by this company name</label>
                                </div>
                            </div>

                            <div class="error_holder"></div>
                            <input type="button" class="btn btn-info w-100" id="btnbookit" value="Book IT!"/>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-4">
                <h4 class="text-center">Calendar</h4>
                <!--                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                    Cancel/LateCheckout
                                </button>-->
            </div>
        </div>
        <hr class="bottom-space">
    </div>
</section>
<div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Edit My Reservation</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form id="editReservation">
                    <input type="hidden" id="resid" name="resid"/>
                    <div class="form-group">
                        <select class="form-control" id="sltlist" name="sltlist">
                            <option value="0" selected disabled>Please select an option</option>
                            <option value="1">Cancel Reservation</option>
                            <option value="2">Request late Checkout</option>
                        </select>
                    </div>
                    <div class="form-group" style="display:none" id="editcomments">
                        <label for="txteditresarea">Please leave a comment below</label>
                        <textarea class="form-control rounded-0" id="txteditresarea" name="txteditresarea" rows="5"></textarea>
                    </div>
                </form>
                <div class="editErrHolder"></div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info" id="btnconfirmedit">Confirm</button>
            </div>

        </div>
    </div>
</div>

<?php

require_once 'common/footer.php';
