<?php

// Javascript/Ajax Calls to this page for quering the DB and returning Results
require_once '../core/lib/database.php';
//switch ($_GET['action']) {
//    
//}

switch ($_POST['action']) {
    case 'load':
        echo json_encode(load());
        break;
    case 'searchRooms':
        echo json_encode(searchRooms());
        break;
    case 'searchRef':
        echo json_encode(searchRef());
        break;
    case 'editReservation':
        echo json_encode(editReservation());
        break;
    case 'submitBooking':
        echo json_encode(submitBooking());
        break;
}

function submitBooking() {
    require '../model/reservations/reservationsmodel.php';
    require '../model/customer/customermodel.php';
    $res = new reservationsmodel();
    $cs = new customermodel();

    parse_str($_POST['form'], $booking);
//    Check if customer Exists?
    $csid = '0';

    if (empty($cst = $cs->getCustomerbyEmail($booking['email']))) {
//        Create customer and set csid for reservation reference
        $cs->createCustomer($booking['fname'], $booking['lname'], $booking['email'], $booking['contactno']);
        $customer = $cs->getCustomerbyEmail($booking['email']);
        $csid = $customer->csid;
    } else {
        $customer = $cs->getCustomerbyEmail($booking['email']);
        $csid = $customer->csid;
    }
//    Create reservation and ref numbers
    if ($booking['chkTerms'] === 'on' && $booking['chkdeparrival'] === 'on') {
        $terms = 1;
        $aterms = 1;
//    Bug here if books again on the same day
//    @TODO::FIX REF BUG
        $ref = $csid . date('Ymd');
    }

    $call = $res->createReservation($booking['sltroomtype'], $csid, $terms, $booking['txtfrom'], $booking['txtto'], $aterms, $booking['rdrInline'], $ref);

    if (!$call) {
//        This is for the page status, i didnt want to clear all the fields and make another function as im already late.
//        return array('success' => false, 'status' => 'danger', 'status_heading' => 'There was an error', 'status_body' => 'Oops! There seemed to be an error creating your reservation. Please contact us with the Error Code CODE: SB001');
        return['succcess' => false];
    } else {
        return['success' => true, 'ref' => $ref];
//        This is for the page status, i didnt want to clear all the fields and make another function as im already late.
//        return array('success' => true, 'status' => 'success', 'status_heading' => 'Success - Reservation Recieved', 'status_body' => 'Your reservation was recieved and is currently being processed. Your Reference number is ' . $ref . '. Please use this as a reference when contacting us. *View ref search above.');
    }
}

function editReservation() {
    require '../model/reservations/reservationsmodel.php';
    $res = new reservationsmodel();
//    parse_str($_POST['form']) 
//    Leaving it like this you would need to ensure your vars going in are exactly the same as the front end name in your php file. E.g name="john" editres($john)
    parse_str($_POST['form'], $edit);
    $resid = $edit['resid'];
    $opt = $edit['sltlist'];

    if ($opt === "1") {
        $coms = $edit['txteditresarea'];
        $comments = 'Cancelled Reservation ' . ' Comments: ' . $coms;
    } else {
        $comments = 'Requested Late Checkout';
    }
//    Deliberate overwrite of comments if not, mysql Query would have a concat in the comments section. User can only pick one item not both.
    $call = $res->updateReservationCommentsById($resid, $comments);
    if (!$call) {
        return ['success' => false];
    } else {
        return ['success' => true];
    }
}

function searchRef() {
    require '../model/reservations/reservationsmodel.php';
    $res = new reservationsmodel();
    $reference = [];
    $refNumber = $_POST['ref'];
    $call = $res->getReservationByRef($refNumber);
    if (!empty($call)) {
        foreach ($call as $data) {
            $name = ucfirst($data->csfirstname) . ' ' . ucfirst($data->cslastname);
            $room = $data->rname . ' - @ R' . number_format($data->ramount, 2) . ' Per Night';
            $comments = $data->rescomments;
            if ($comments === NULL) {
                $comments = 'No comments added';
            }
            $reference[] = [
                'Name' => utf8_encode($name),
                'RoomBooked' => utf8_encode($room),
                'ArrivalDate' => utf8_encode($data->resarrdate),
                'DepartureDate' => utf8_encode($data->resdepdate),
                'Payment' => utf8_encode($data->resptype),
                'Comments' => utf8_encode($comments),
                'TotalAmount' => utf8_encode($data->days . ' X ' . number_format($data->ramount, 2) . ' = R' . number_format($data->TotalFee)),
                'action' => '<a href="javscript:void(0)" data-toggle="modal" data-target="#myModal" style="margin-right: 10px;" class="far fa-edit control-set-resedit" data-resid="' . $data->resid . '"/>'
            ];
        }
        return ['success' => true, 'tblref' => $reference];
    } else {
        return ['success' => false];
    }
}

function searchRooms() {
    require '../model/rooms/roomsmodel.php';
    $rm = new roomsmodel();
    $from = $_POST['arrival'];
    $to = $_POST['departure'];
    $call = $rm->getAvailableRooms($from, $to);
    if (!$call || empty($call)) {
        return['success' => false];
    } else {
        $html = '<option selected disabled>Room Type</option>';
        foreach ($call as $data) {
            $html .= '<option value="' . $data->rid . '">' . $data->rname . '(' . $data->rcapacity . ' Sleeper) @ R ' . number_format($data->ramount, 2) . ' per day</option>';
        }

        return['success' => true, 'tblrooms' => $html];
    }
}

function load() {
    require '../model/customer/customermodel.php';
    require '../model/reservations/reservationsmodel.php';
    require '../model/rooms/roomsmodel.php';
    $rm = new roomsmodel();
    $cm = new customermodel();
    $res = new reservationsmodel();


    $customers = [];
    $rooms = [];
    $reservations = [];

//    Get all Customers
    foreach ($cm->getAllCustomers() as $data) {
        $customers[] = [
            'CustomerID' => utf8_encode($data->csid),
            'CustomerFirstName' => utf8_encode($data->csfirstname),
            'CustomerLastName' => utf8_encode($data->cslastname),
            'CustomerEmail' => utf8_encode($data->csemail),
            'CustomerMobile' => utf8_encode($data->csmobile)
        ];
    }

//    Get all rooms
    foreach ($rm->getAllRooms() as $data) {
        $rooms[] = [
            'RoomID' => utf8_encode($data->rid),
            'RoomName' => utf8_encode($data->rname),
            'RoomCapacity' => utf8_encode($data->rcapacity),
            'RoomAmount' => utf8_encode($data->ramount)
        ];
    }

//    Get all reservations
    foreach ($res->getAllReservations() as $data) {
//            Should add this to DB Sql query As a Case statement to show yes or no
        if ($data->resterms === '1') {
            $terms = 'Yes';
        }else{
            $terms = 'No';
        }
        if ($data->resdeparrival === '1') {
            $deparrival = 'Yes';
        }else{
            $deparrival = 'No';
        }
        
        $reservations[] = [
            'ReservationID' => utf8_encode($data->resid),
            'ReservationReference' => utf8_encode($data->resref),
            'ReservationCustomerID' => utf8_encode($data->rescsid),
            'ReservationRoomID' => utf8_encode($data->resrid),
            'ReservationArrivalDate' => utf8_encode($data->resarrdate),
            'ReservationDepartureDate' => utf8_encode($data->resdepdate),
            'ReservationCancelled' => utf8_encode($data->rescancelled),
            'ReservationComments' => utf8_encode($data->rescomments),
            'arrTimes' => utf8_encode($deparrival),
            'terms' => utf8_encode($terms),
            'ReservationCreatedDate' => utf8_encode($data->rescreateddate),
        ];
    }

    return ['customertable' => $customers, 'roomstable' => $rooms, 'reservationstable' => $reservations];
}
