<?php

Class SafePDO extends PDO {

    public static function exception_handler($exception) {
        // Output the exception details
        die('Uncaught exception: ' . $exception->getMessage());
    }

    public function __construct($dsn, $username = '', $password = '', $driver_options = array()) {

        // Temporarily change the PHP exception handler while we . . .
        set_exception_handler(array(__CLASS__, 'exception_handler'));

        // . . . create a PDO object
        parent::__construct($dsn, $username, $password, $driver_options);

        // Change the exception handler back to whatever it was before
        restore_exception_handler();
    }

}

class database {

    private $db;

    function __construct() {
        if (!isset($this->db)) {
            $this->db = new SafePDO("mysql:host=localhost;dbname=simplereservations", 'root', '');
        }
    }

    function get_single_row($basecommand, $params) {
        $stmt = $this->db->prepare($basecommand);
        $stmt->execute($params);
        $res = $stmt->fetch(PDO::FETCH_OBJ);
        return $res;
    }

    function get_returned_rows($basecommand, $params) {
        $stmt = $this->db->prepare($basecommand);
        $stmt->execute($params);
        return $stmt->rowCount();
    }

    function get_data_set($basecommand, $params) {
        try {
            $stmt = $this->db->prepare($basecommand);
            $stmt->execute($params);
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        } catch (PDOException $err) {
            error_log($err->getMessage());
            return false;
        }
    }

    function update_db($basecommand, $params) {
        try {
            $stmt = $this->db->prepare($basecommand);
            $result = $stmt->execute($params);
            return $result;
        } catch (PDOException $err) {
            echo $err->getMessage();
            return false;
        }
    }

    function call_stored_procedure($procname, $params, $fetch = false) {
        try {
            $stmt = $this->db->prepare('CALL ' . $procname);

            if ($fetch == true || $params === null) {
                $stmt = $this->db->prepare('CALL ' . $procname);
                $stmt->execute($params);
                $res = $stmt->fetch();
                return $res;
            } else {
                foreach ($params as $item) {
                    $stmt->bindParam($item['index'], $item['value'], $item['type']);
                }
                return $stmt->execute();
            }
        } catch (PDOException $err) {
            print($err->getMessage());
            return false;
        }
    }

    function call_stored_procedureobj($procname, $params, $fetch = false) {
        try {
            $stmt = $this->db->prepare('CALL ' . $procname);

            if ($fetch == true || $params === null) {
                $stmt = $this->db->prepare('CALL ' . $procname);
                $stmt->execute($params);
                $res = $stmt->fetchAll(PDO::FETCH_OBJ);
                return $res;
            } else {
                foreach ($params as $item) {
                    $stmt->bindParam($item['index'], $item['value'], $item['type']);
                }
                return $stmt->execute();
            }
        } catch (PDOException $err) {
            print($err->getMessage());
            return false;
        }
    }

    function __destruct() {
        $this->db = null;
        unset($this->db);
    }

}
