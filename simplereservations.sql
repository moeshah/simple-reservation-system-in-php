/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 100128
Source Host           : localhost:3306
Source Database       : simplereservations

Target Server Type    : MYSQL
Target Server Version : 100128
File Encoding         : 65001

Date: 2019-05-31 15:35:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `csid` smallint(6) NOT NULL AUTO_INCREMENT,
  `csfirstname` varchar(20) DEFAULT NULL,
  `cslastname` varchar(20) DEFAULT NULL,
  `csemail` varchar(50) DEFAULT NULL,
  `csmobile` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`csid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES ('1', 'mohammed', 'shah', 'emsm007@gmail.com', '0815014527');

-- ----------------------------
-- Table structure for reservations
-- ----------------------------
DROP TABLE IF EXISTS `reservations`;
CREATE TABLE `reservations` (
  `resid` smallint(6) NOT NULL AUTO_INCREMENT,
  `resref` varchar(20) DEFAULT NULL,
  `rescsid` smallint(6) DEFAULT NULL,
  `resrid` tinyint(6) DEFAULT NULL,
  `resarrdate` date DEFAULT NULL,
  `resdepdate` date DEFAULT NULL,
  `resptype` varchar(15) DEFAULT NULL,
  `rescomments` longtext,
  `resterms` tinyint(1) DEFAULT NULL,
  `resdeparrival` tinyint(1) DEFAULT NULL,
  `rescancelled` tinyint(2) DEFAULT NULL,
  `rescreateddate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`resid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reservations
-- ----------------------------
INSERT INTO `reservations` VALUES ('1', '120190531', '1', '1', '2019-05-31', '2019-06-02', 'Credit Card', null, '1', '1', '0', '2019-05-31 15:34:41');

-- ----------------------------
-- Table structure for rooms
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
  `rid` tinyint(4) NOT NULL AUTO_INCREMENT,
  `rname` varchar(20) DEFAULT NULL,
  `rcapacity` smallint(6) DEFAULT NULL,
  `ramount` decimal(10,0) DEFAULT NULL,
  `ravailable` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rooms
-- ----------------------------
INSERT INTO `rooms` VALUES ('1', 'Single Room', '2', '500', '0');
INSERT INTO `rooms` VALUES ('2', 'Double Room', '4', '950', '0');
INSERT INTO `rooms` VALUES ('3', 'Deluxe Room', '6', '1350', '0');
