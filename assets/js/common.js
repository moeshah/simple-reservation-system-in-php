(function ($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 56)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function () {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 56
    });

})(jQuery); // End of use strict

var date = new Date();
date.setDate(date.getDate());

$('.datepicker').datepicker({ 
    startDate: date,
    minDate: date,
    dateFormat: 'yy-mm-dd'
});


//Loader Implementation
//$(document).ready(function () {
//    var minPageHeight = window.innerHeight - $('nav').outerHeight() - $('footer').outerHeight() - 20;
//    $('body > .container').eq(0).css({
//        'min-height': minPageHeight
//    });
//    $(document)
//            .ajaxStart(function () {
//                $('#loaderRadio').show();
//            })
//            .ajaxStop(function () {
//                $('#loaderRadio').hide();
//            });
//});
//Page Status, View at the end of a booking.
function pageStatus(data, status, status_heading, status_body) {

    var errorContainer = $('#pagestatus_container');
    var header = $('#pagestatus_header', errorContainer);
    var body = $('#pagestatus_body', errorContainer);

    if (data == 'hide') {
        errorContainer.hide();
        return;
    } else if (data === 0) {
        data = {};
        data.status = status;
        data.status_heading = status_heading;
        data.status_body = status_body;
    }

    //Handle page redirection if not logged in TODO
//    if (typeof data.redirect === 'undefined' || data.redirect === null) {
//
//    }

    errorContainer.removeClass('alert-primary alert-secondary alert-success alert-danger alert-warning alert-info alert-light alert-dark');
    header.empty();
    body.empty();

    switch (data.status) {
        case true:
            errorContainer.addClass('alert-success');
            break;
        case false:
            errorContainer.addClass('alert-danger');
            break;
        case 'primary':
            errorContainer.addClass('alert-primary');
            break;
        case 'secondary':
            errorContainer.addClass('alert-secondary');
            break;
        case 'success':
            errorContainer.addClass('alert-success');
            break;
        case 'danger':
            errorContainer.addClass('alert-danger');
            break;
        case 'warning':
            errorContainer.addClass('alert-warning');
            break;
        case 'info':
            errorContainer.addClass('alert-info');
            break;
        case 'light':
            errorContainer.addClass('alert-light');
            break;
        case 'dark':
            errorContainer.addClass('alert-dark');
            break;
    }

    header.html(toTitleCase(data.status_heading));
    body.html(capitalizeFirstLetter(data.status_body));
    errorContainer.show();
    if (!isScrolledIntoView(errorContainer)) {
        scrollTo($('html,body'));
    }
}

//Personal Validation File Insert
function validation() {

    $('body input').each(function (event) {
        //Character Filter Out Non Digits
        if ($(this).attr('data_val_type') === 'cellno' || $(this).attr('data_val_type') === 'telno') {
            var input = $(this);
            input.on('keyup', function () {
                var n = $(this).val().replace(/[^\d\.]/g, '');
                $(this).val(n.toLocaleString());
            });
        }
        //Remove errors if checked
        if ($(this).is(':checkbox')) {
            $(this).on('change', function () {
                if ($(this).is(':checked')) {
                    $(this).siblings('.validation_error,.validation_info').remove();
                }
            });
        }
        //Ensure Number only fields
        if ($(this).attr('data_val_type') === 'number') {
            $(this).on('keyup', function () {
                var n = $(this).val().replace(/[^\d\.]/g, '');
                $(this).val(n.toLocaleString());
            });
        }
    });

    $('body select').on('change', function () {
        var select = $(this),
                select_value = select.val(),
                select_type = select.attr('data_val_type'),
                select_targetinput = $('#' + select.attr('data_val_input')),
                select_notrequired = select.attr('data_not_required');
        select_alertfooter = select.closest('.card').find('.alert-dismissiable');

        select.siblings('.validation_error,.validation_info').remove();
        select_targetinput.siblings('.validation_error,.validation_info').remove();

        if (select_notrequired === "1") {
            return;
        }
//Close large alert message at the bottom
        select_alertfooter.alert('close');

        if (select_targetinput.length === 1) {
            if (select_targetinput.val().length === "") {

            }
        }

        if (select_targetinput.length !== 0) {
            if (select_targetinput.val().length !== 0) {
                select_targetinput.trigger('change');
            }
        }

        if (select_value === null) {
            select.parent().append('<span class="alert alert-danger validation_error">Please select an option</span>');
        }

        switch (select_type) {
            case 'idno':
                if (select_value === 0) {
                    select_targetinput.prop("disabled", true);
                    select_targetinput.attr("placeholder", "Please Select Document Type");
                } else if (select_value === 'pass') {
                    select_targetinput.prop("disabled", false);
                    select_targetinput.attr("placeholder", "Passport Number");
                    //Turn off filter
                    select_targetinput.off('keyup');
                } else if (select_value === 'rsaid') {
                    select_targetinput.prop("disabled", false);
                    select_targetinput.attr("placeholder", "Identification Number");
                    //Filter non digits
                    select_targetinput.on('keyup', function () {
                        var n = select_targetinput.val().replace(/[^\d\.]/g, '');
                        select_targetinput.val(n.toLocaleString());
                    });
                }
                break;
            default:
                break;
        }
    });

    $('body input').on('focusout change', function () {
        var input = $(this),
                input_value = input.val(),
                input_type = input.attr('data_val_type'),
                input_targetselect = $('#' + input.attr('data_val_select')),
                input_minlength = input.attr('data_val_minlength'),
                input_notrequired = input.attr('data_not_required'),
                input_length = input_value.length,
                input_length_text = (input_length === 1) ? input_length + ' character' : input_length + ' characters',
                input_dependant = $('#' + input.attr('data_val_dependant'));
        input_alertfooter = input.closest('.card').find('.alert-dismissiable');
        input.siblings('.validation_error,.validation_info').remove();

        if (input_notrequired === "1" && input_value === "") {
            return;
        }

        input_alertfooter.alert('close');
        switch (input_type) {
            case 'name':
                if (input_notrequired === '1') {
                    var errortype = "alert-info validation_info";
                } else {
                    var errortype = "alert-danger validation_error";
                }
                if (input_minlength !== null && input_minlength !== undefined) {
                    if (input_length < input_minlength) {
                        input.parent().append('<span class="alert ' + errortype + '">Name too short</span>');
                    }
                } else if (input_length < 2) {
                    input.parent().append('<span class="alert ' + errortype + '">Name too short</span>');
                } else if (input_length === 0) {
                    input.parent().append('<span class="alert ' + errortype + '">Blank</span>');
                }
                break;
            case 'surname':
                if (input_notrequired === '1') {
                    var errortype = "alert-info validation_info";
                } else {
                    var errortype = "alert-danger validation_error";
                }
                if (input_minlength !== null && input_minlength !== undefined) {
                    if (input_length < input_minlength) {
                        input.parent().append('<span class="alert ' + errortype + '">Lastname too short</span>');
                    }
                } else if (input_length < 2) {
                    input.parent().append('<span class="alert ' + errortype + '">Lastname too short</span>');
                } else if (input_length === 0) {
                    input.parent().append('<span class="alert ' + errortype + '">Blank</span>');
                }
                break;
            case 'number':
                if (input_length < input_minlength) {
                    input.parent().append('<span class="alert alert-danger validation_error">Too few numbers, expected at least ' + input_minlength + '</span>');
                }
                break;
            case 'idno':
                if (input_targetselect.val() === null) {
                    input.parent().append('<span class="alert alert-danger validation_error">Please select a Document Type</span>');
                } else if (input_length === 0) {
                    input.parent().append('<span class="alert alert-danger validation_error">Cannot be blank</span>');
                } else if (input_targetselect.val() === 'pass') {
                    var possible_digits = input_value.replace(/[^\d\.]/g, '');
                    if (input_length < 5) {
                        input.parent().append('<span class="alert alert-danger validation_error">' + input_length_text + ' entered. Please enter your full Passport Number</span>');
                        //TODO Some soft of validation on passports
                    } else if (possible_digits.length === 13) {
                        input.parent().append('<span class="alert alert-info validation_info">Are you entering an RSA ID number? Current set Document Type is Passport. Please ensure this is not an error as it will possibly lead to your application getting declined');
                    }
                } else if (input_targetselect.val() === 'rsaid') {
                    var check = validateRSAID(input_value);
                    if (input_length !== 13) {
                        input.parent().append('<span class="alert alert-danger validation_error">' + input_length_text + ' entered. Please enter your 13 Digit RSA Identification Number</span>');
                    } else if (!check.valid) {
                        input.parent().append('<span class="alert alert-danger validation_error">Error : ' + check.message + '</span>');
                    }
                }
                break;
            case 'telno':
                if (input_length < 10) {
                    input.parent().append('<span class="alert alert-danger validation_error">' + input_length_text + ' entered. Please enter your full 10 Digit Contact Number</span>');
                } else if (input_length > 15) {
                    input.parent().append('<span class="alert alert-danger validation_error">' + input_length_text + ' entered. Contact number too long</span>');
                } else if (input_length > 10) {
                    input.parent().append('<span class="alert alert-info validation_info">' + input_length_text + ' entered. Expected 10, is this correct?</span>');
                }
                break;
            case 'cellno':
                var prefix = input_value.substring(0, 2);
                var suffix = input_value.substring(2);
                if (input_length < 10) {
                    input.parent().append('<span class="alert alert-danger validation_error">' + input_length_text + ' entered. Please enter your full Celluar Number</span>');
                } else if (input_length > 15) {
                    input.parent().append('<span class="alert alert-danger validation_error">' + input_length_text + ' entered. Contact number too long</span>');
                } else if (prefix !== '06' && prefix !== '07' && prefix !== '08') {
                    input.parent().append('<span class="alert alert-info validation_info">Is this a foreign number cell number? Problem found at <strong>' + prefix + '</strong>' + suffix);
                } else if (input_length > 10) {
                    input.parent().append('<span class="alert alert-info validation_info">' + input_length_text + ' entered. Expected 10, is this correct?</span>');
                }
                break;
            case 'email':
                input_value = input_value.replace(/\s/g, '');
                input.val(input_value);
                if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(input_value)) {
                    input.parent().append('<span class="alert alert-danger validation_error">Please enter a valid email address. Example: name@domain.co.za</span>');
                }
                break;
            case 'address':
                if (input_length === 0) {
                    input.parent().append('<span class="alert alert-danger validation_error">Please ensure you have entered the full address</span>');
                }
                break;
            case 'tcs':
                var tcs = input.closest('.tcs');
                if (input_notrequired === '1') {
                    return;
                }
                if (input.prop('checked')) {
                    $('.validation_error', tcs).remove();
                } else {
                    if ($('.validation_error', tcs).length === 0) {
                        input.closest('.form-check').append('<div class="alert alert-danger validation_error alert-dismissible show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><a href="#' + tcs.attr("id") + '" class="alert-link">Do you consent to the above?</a> Please confirm you have read and agree to the above by clicking the checkbox.</div>');
                    }
                }
                break;
            case 'document':
                var limit = 5242880; //5MB Limit
                var messageText = '';
                var file = input[0].files;

                if (file.length === 0) {
                    messageText = 'No files currently selected for upload';
                    input.parent().append('<span class="alert alert-info validation_info">' + messageText + '</span>');
                } else {
                    if (validFileType(file[0])) {
                        var sizeText = returnReadableSize(file[0].size);
                        if (file[0].size <= limit) {
                            messageText = 'File selected. File name ' + file[0].name + ', File size ' + sizeText + '.';
                            input.parent().append('<span class="alert alert-info validation_info">' + messageText + '</span>');
                        } else {
                            messageText = 'File name ' + file[0].name + ', File size ' + sizeText + '. File too large, must be less than 5MB';
                            input.parent().append('<span class="alert alert-danger validation_error">' + messageText + '</span>');
                        }
                    } else {
                        messageText = 'File name ' + file[0].name + ': Not a valid file type. Please choose a valid file type. Supported File Types : PNG, JPG, JPEG, GIF, PDF';
                        input.parent().append('<span class="alert alert-danger validation_error">' + messageText + '</span>');
                    }
                }
                var fileArray = [];
                //Get all loaded documents and create array of names
                $('body').find('[data_val_type="document"]').each(function () {
                    if ($(this).val().length > 0) {
                        fileArray.push($(this).val());
                    }
                });

                for (var i = 0; i < fileArray.length - 1; i++) {
//                    search through array
                    if (fileArray[i + 1] === fileArray[i]) {
//                        compare with self and one ahead for duplication
                        if (input_value === fileArray[i]) {
//                            if duplication show error if applies to current input
                            input.parent().append('<span class="alert alert-info validation_info">Document of this name already selected in another option</span>');
                        }
                    }
                }
                break;
            case 'password':
                var lowerCaseLetters = /[a-z]/g;
                var upperCaseLetters = /[A-Z]/g;
                var numbers = /[0-9]/g;
                if (!input_value.match(numbers)) {
                    input.parent().append('<span class="alert alert-danger validation_error">Must contain at least one number</span>');
                }
                if (!input_value.match(upperCaseLetters)) {
                    input.parent().append('<span class="alert alert-danger validation_error">Must contain at least one lower case letter</span>');
                }
                if (!input_value.match(lowerCaseLetters)) {
                    input.parent().append('<span class="alert alert-danger validation_error">Must contain at least one upper case letter</span>');
                }
                break;
            case 'confirmpassword':
                if (input_value !== input_dependant.val()) {
                    input.parent().append('<span class="alert alert-danger validation_error">Password does not match</span>');
                }
                break;
            default:

                break;
        }
    });
}

function validateData(elem) {
    success = true;
    var card = elem.closest('.card');

    //Empty and hide error holder
    $('.error_holder', card).hide();
    $('.error_holder', card).empty();
    
    $('input', card).each(function () {
        if ($(this).attr("data_val_required") == undefined) {
            $(this).trigger('change');
        }
    });

    $('select', card).each(function () {
        if ($(this).attr("data_val_required") == undefined) {
            $(this).trigger('change');
        }
    });
    if ($('.validation_error', card).length !== 0) {
        success = false;
    }


    $('.error_holder', card).html('<div class="alert alert-info alert-dismissiable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><p>There is an issue with the following fields:</p><p>You may close this message or try again to update it if there are any errors</p><ul class="error_list"></ul></div>');
    if (!success) {
        $('.validation_error', card).each(function () {
            $('.error_list', card).append('<li>' + $(this).siblings('label').text() + '</li>');
        });
        $('.error_holder', card).show();
    }
    return success;
}