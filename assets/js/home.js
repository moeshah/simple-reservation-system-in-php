//Comments Everywhere!!
$(document).ready(function () {
    validation();
    loadTables();
    load();

    $('#btnshowDbtbls').on('click', function (e) {
        e.preventDefault();
        $('#tbls').toggle();
    });

    $(document).on('click', '.control-set-resedit', function () {
//      Sets Modal to the Refid from the DB for update and ensures first Entrance UI
        $("#resid").val($(this).attr('data-resid'));
        $('#editcomments').css({display: 'none'});
        $('#txteditresarea').val('');
    });

    $('#sltlist').on('change', function (e) {
//        Toggles the Comments block based on the change of the dropdown list
        if ($(this).val() === "1") {
            $('#editcomments').css({display: 'block'});
        } else {
            $('#editcomments').css({display: 'none'});
        }
    });

    $('#btnconfirmedit').on('click', function (e) {
//        I wouldve also disabled the confirm button until the textarea is filled by checking the trimmed value length of the text area on keyup or keydown
//        Checks if its the cancelled Dropdown
        if ($('#sltlist option:selected').val() === "1") {
//                Trims and checks value length to ensure its not empty or filled with white space
            if ($('#txteditresarea').val().trim().length >= 4) {
                editReservation();
            } else {
//                Error Holder Bootstrap Alert Danger, my personal Favorite
                $('.editErrHolder').empty().html('<div class="form-group text-center"><div class="alert alert-danger"><strong>Oops!</strong> Please Leave us a comment</div></div>').delay(3000).fadeOut('slow');
                $('.editErrHolder').css({display: 'block'});
            }
        } else {
//            If its not the Cancelled Option, no need to check the text area
            editReservation();
        }
    });

//    Removed, as the toggle was buggy
//    $('#srchphrase').on('blur', function (e) {
//        if ($(this).val().length > 4) {
//            searchReference();
//        }
//    });


    $("#btnRoomSearch").on('click change', function () {

        if ($(".datepicker").val().length === 0) {
            $('.datepicker').css({"border-color": "red",
                "border-width": "1.5px",
                "border-style": "solid"});
            $('.errorDates').empty().html('<div class="form-group text-center"><div class="alert alert-danger"><strong>Oops!</strong> Select a date Range</div></div>').delay(5000).fadeOut('slow');
            $('.errorDates').css({display: 'block'});
            $('.crddetails').css({display: 'none'});
            return false;
        }

        var arrdate = new Date($('#txtarrdate').val());
        var depdate = new Date($('#txtdepdate').val());


        if (Date.parse(arrdate) > Date.parse(depdate)) {
            $('.errorDates').empty().html('<div class="form-group text-center"><div class="alert alert-danger"><strong>Oops!</strong> Arrival Date cannot be after Departure Date</div></div>').delay(5000).fadeOut('slow');
            $('.errorDates').css({display: 'block'});
            $('#txtarrdate').css({"border-color": "red",
                "border-width": "1.5px",
                "border-style": "solid"});
            $('#txtdepdate').css({"border-color": "#ced4da",
                "border-width": "1px",
                "border-style": "solid"});
            $('.crddetails').css({display: 'none'});
            return false;
        } else {
//            remove manually errors 

            $('.errorDates').css({display: 'none'});
            $('#txtdepdate').css({"border-color": "#ced4da",
                "border-width": "1px",
                "border-style": "solid"});
            $('#txtarrdate').css({"border-color": "#ced4da",
                "border-width": "1px",
                "border-style": "solid"});
//            Set hidden params for selected dates, if a client changes the dates he needs to press search rooms for it to reflect or we take the initial selected dates
            $('#txtfrom').val($('#txtarrdate').val());
            $('#txtto').val($('#txtdepdate').val());
            $('#lblroomtype').text('Please select a room type from' + $('#txtfrom').val() + ' to ' + $('#txtto').val());
//            Valid Data Show available rooms
            searchRooms();
        }
    });


//Main Buttons Start
    $('#srchphrase').keypress(function (e) {
        var key = e.which;
        if (key === 13) {
//            Enter Key pressed is the simplest and to disable the field
            $('#srchphrase').attr('disabled', true);
            searchReference();
        }
    });

    $('#btnbookit').on('click', function (e) {
        e.preventDefault();

//        Using a different straight forward validation for the radio buttons and the check boxes
        var paymentType = '0';
        $('.form-rdr').css({'color': '#212529'});
//        JS
        var chkdeparrival = document.getElementById('chkdeparrival').checked;
//        Jquery
        var tcs = $('#chkTerms').is(':checked');
        if ($('input[type=radio][name=rdrInline]:checked').length === 0) {
            alert("Please select a payment type. (Lazy Amateur Validation, also view how untidy the code gets)");
            $('.form-rdr').css({'color': 'red'});
            paymentType = '1';
            return;
        }

//            If we do not return within these if blocks, our validateData function will remove the .appended error blocks.
        if (!chkdeparrival) {
            $('#chkdeparrival').closest('.custom-checkbox').append('<div class="alert alert-danger validation_error alert-dismissible show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Do you consent to the above? Please confirm you have read and agree to the above by clicking the checkbox.</div>');
            return false;
        } else if (!tcs) {
            $('#chkTerms').closest('.custom-checkbox').append('<div class="alert alert-danger validation_error alert-dismissible show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Please confirm you have read and agree to the above by clicking the checkbox.</div>');
            return false;
        }

        if (validateData($(this)) && paymentType === '0' && chkdeparrival && tcs) {
//            fun part, call to DB and ref creator etc etc
            submitBooking();
        }
    });
//Main Buttons End
});

function submitBooking() {
    $.ajax({
        url: '../ajax/home_ajax.php',
        dataType: 'JSON',
        type: 'POST',
        data: {
            action: 'submitBooking',
            form: $('#frmbookabed').serialize()
        },
        success: function (data) {
            if (!data) {
                swal({
                    type: 'warning',
                    title: 'Request Not Completed',
                    text: 'Something went wrong.. We are looking into it Code(SB001)',
                    footer: 'Please Contact us via our numbers or contact page'
                });
            } else {
                swal({
                    title: "Congratulations on making your booking!",
                    text: "Please use your Refernce number to search for your booking " + data.ref + ". You are being redirected Please Wait..",
                    type: 'success',
                    closeOnClickOutside: false,
                    timer: 10000,
                    buttons: false
                });
//                Reloads current page as im not a fan of clearing fields when done, so the easy way out is just to reload the page
                setTimeout(function () {
                    location.reload();
                }, 5000);
            }
        }
    });
}

function editReservation() {
//    If user cancels reservation, only backend can set the cancelled column as there must be some criteria to be met like cant cancel on the day etc etc
    $.ajax({
        url: '../ajax/home_ajax.php',
        dataType: 'JSON',
        type: 'POST',
        data: {
            action: 'editReservation',
            form: $('#editReservation').serialize()
        },
        success: function (data) {
            if (!data) {
                swal({
                    type: 'warning',
                    title: 'Request Not Completed',
                    text: 'Something went wrong.. We are looking into it Code(ER001)',
                    footer: 'Please Contact us via our numbers or contact page'
                });
            } else {
//                Deliberate did not run the 
//                load(); Function as it will conflict with the reloading of the searchReference(); function
                $('#myModal').modal('hide');
                swal("Request Completed!", "We have received your query and will get back to you!", "success");
                $("#sltlist").val("0");
                searchReference();
            }
        }
    });
}

function searchReference() {
    $.ajax({
        url: '../ajax/home_ajax.php',
        dataType: 'JSON',
        type: 'POST',
        data: {
            action: 'searchRef',
            ref: $('#srchphrase').val()
        },
        success: function (data) {
            if (!data.success) {
                $('.errHolder').empty().html('<div class="form-group text-center"><div class="alert alert-danger"><strong>Oops!</strong> Your Ref Number Cannot be found. Please try Again</div></div>');
                $('#srchres').css({display: 'none'});
                $('.errHolder').css({display: 'block'});
                $('#srchphrase').attr('disabled', false);
            } else {
                $('#srchphrase').attr('disabled', false);
                var t = $('#tblref').DataTable();
                t.clear();
                t.rows.add(data.tblref);
                t.columns.adjust();
                t.draw();
                $('#srchres').css({display: 'block'});
                $('.errHolder').css({display: 'none'});
            }
        }
    });
}

function searchRooms() {
    $.ajax({
        url: '../ajax/home_ajax.php',
        dataType: 'JSON',
        type: 'POST',
        data: {
            action: 'searchRooms',
            arrival: $('#txtarrdate').val(),
            departure: $('#txtdepdate').val()
        },
        success: function (data) {
            console.log(data);
            if (!data.success) {
                $('.errorDates').empty().html('<div class="form-group text-center"><div class="alert alert-danger"><strong>Oops!</strong> No Rooms Available for the given date range</div></div>').delay(3000).fadeOut('slow');
                $('.errorDates').css({display: 'block'});
                $('.crddetails').css({display: 'none'});
            } else {
                $('.errorDates').css({display: 'none'});
                $('#sltroomtype').empty().html(data.tblrooms);
                $('.crddetails').css({display: 'block'});
            }
        }
    });
}

function load() {
    $.ajax({
        url: '../ajax/home_ajax.php',
        dataType: 'JSON',
        type: 'POST',
        data: {
            action: 'load'
        },
        success: function (data) {
            var t = $('#tblrooms').DataTable();
            t.clear();
            t.rows.add(data.roomstable);
            t.columns.adjust();
            t.draw();
            var y = $('#tblreservations').DataTable();
            y.clear();
            y.rows.add(data.reservationstable);
            y.columns.adjust();
            y.draw();
            var r = $('#tblcustomer').DataTable();
            r.clear();
            r.rows.add(data.customertable);
            r.columns.adjust();
            r.draw();
        }
    });
}

function loadTables() {
    $('#tblref').DataTable({
        paging: false,
        order: [],
        info: false,
        searching: false,
        columns: [
            {data: 'Name'},
            {data: 'RoomBooked'},
            {data: 'ArrivalDate'},
            {data: 'DepartureDate'},
            {data: 'Payment'},
            {data: 'Comments'},
            {data: 'TotalAmount'},
            {data: 'action'}
        ],
        language: {emptyTable: "No Data"}
    });
    $('#tblrooms').DataTable({
        paging: false,
        order: [],
        info: false,
        columns: [
            {data: 'RoomID'},
            {data: 'RoomName'},
            {data: 'RoomCapacity'},
            {data: 'RoomAmount'}
        ],
        language: {emptyTable: "No Data"}
    });

    $('#tblcustomer').DataTable({
        paging: false,
        order: [],
        info: false,
        columns: [
            {data: 'CustomerID'},
            {data: 'CustomerFirstName'},
            {data: 'CustomerLastName'},
            {data: 'CustomerEmail'},
            {data: 'CustomerMobile'}
        ],
        language: {emptyTable: "No Data"}
    });

    $('#tblreservations').DataTable({
        paging: false,
        order: [],
        info: false,
        columns: [
            {data: 'ReservationID'},
            {data: 'ReservationReference'},
            {data: 'ReservationCustomerID'},
            {data: 'ReservationRoomID'},
            {data: 'ReservationArrivalDate'},
            {data: 'arrTimes'},
            {data: 'ReservationDepartureDate'},
            {data: 'terms'},
            {data: 'ReservationComments'},
            {data: 'ReservationCancelled'},
            {data: 'ReservationCreatedDate'}
        ],
        language: {emptyTable: "No Data"}
    });
}